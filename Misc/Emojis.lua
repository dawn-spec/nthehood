return {

	["Groups"] = {

		[15119632] = { -- Stars

			["[⭐]"] = {1, 2, 3},
			["[🌠]"] = {4},
			["[🌟]"] = {5},
			["[👑]"] = {6, 7, 9, 254, 255},

		},

		[15149792] = { -- Verified/Hall of Fame

			["[☑️]"] = {1, 2, 3},
			["[✅]"] = {4, 253, 254, 255},

		},

	},

	["Players"] = {
		[""] = {265839211}
	},

}