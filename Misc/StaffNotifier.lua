return {

    --// DEVELOPERS
    8195210, -- Benoxa
    93101606, -- iumu
    163721789, -- JokeTheFool

    --// MODERATORS
    3944434729, -- drizzyaudemars
    2395613299, -- dtbbullet
    822999, -- AStrongMuscle
    89473551, -- Luutyy
    155627580, -- AngelicTheWise

    --// JR. MODERATORS
    29242182, -- Ghostlic
    1830168970, -- 512f6 (Faded/fff)
    1085505689, -- 0neLastProphecy

    --// ALTS/OTHERS
    46229910, -- Dragurus
    439942262, -- ReallyCyan
    1553950697, -- TheRealMelonBear
    244844600, -- Nex5us
    525794874, -- RogueDaHoodKing
    3520967, -- FruitySama
    67819707, -- Felku
    2847760384, -- yukipukio
    15427717, -- Sherosama
    16138978, -- zakblak20
    633527, -- SnakeWorl
    990874846, -- Parade_King
    476537893, -- ohDrizzy
    111185547, -- Dio_Brando
    37975670, -- Protall
    16162368, -- Vawntei
    23558830, -- Greed_Ocean
    28357488, -- WashedGarage
    201454243, -- Papa_Mbaye
    34758833, -- NatsuDBlaze
    499728, -- Kenoushi
    30341, -- Juggernaut
    63794379, -- iRenn
    60660789, -- BeeTheKidd
    25717070, -- ppaxxy
    2274607286, -- CrustyMeng

}