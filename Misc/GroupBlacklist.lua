return {

	--// EXPLOITER GROUPS
	10857187, -- Swagmode Community
	12052955, -- Swagmode
	10857159, -- Swag Mode
	13729929, -- Swagmode Entertainment
	12984288, -- Swagmode McDonalds
	8602733, -- IExploit
	7896948, -- IExploit (Buying)
	5485718, -- IExploit (Abandoned)
	9496220, -- RaceCodex Studios
	11906512, -- RaceCodex Community
	14585273, -- RaceCodex. Community
	8022074, -- Raycodex
	15836015, -- Dragonware Community
	12243982, -- ArcticDH
	10598253, -- Nova Gui
	8423554, -- Nova Gui (Old)
	11247010, -- PolarArctic
	7658734, -- Legacy X
	9233423, -- Legacy X Official
	9994296, -- Valiant
	7853899, -- Agent Premium
	15756568, -- Iku
	13388079, -- Iku Scripts
	11663990, -- Iku Ro-Ghoul
	11581735, -- Faded Community
	7285773, -- Faded Vault
	12271076, -- Rayx Community
	15101252, -- Rayx Backup
	15002046, -- CLOTHES EPIC GROUP ! PRO GMAER (Mimic Whitelist)
	14823339, -- SussyX
	12279446, -- KRNL
	14447451, -- Hoodsense Community
	13902228, -- Dimag16
	10607061, -- Da Hood Script Developers
	11850806, -- Xeno Hub
	7095027, -- SPY
	6698825, -- SPY (Buying)
	9783311, -- SPY-WARE
	8090979, -- DankX
	8276237, -- RC-M
	10179344, -- Da Hood Stand Users
	6961646, -- Jump'd
	15826723, -- SYNX V
	9374401, -- CypherBrew
	8179961, -- SamX
	7286260, -- Krnlly
	14193366, -- Stand Users Only
	3528673, -- Infinite Yield
	5370127, -- CMD-X
	12844531, -- SSwaggers
	10565298, -- Trollers Hub v1
	10236818, -- .gg/troll
	12190324, -- Sioxide
	15070691, -- OblivionW
	12670099, -- Exploiters Only
	14092472, -- Galactic Hyperion
	4118953, -- The Exploiters
	8853056, -- Roblox Hackers/Exploiters
	2672890, -- Script Hack
	9014967, -- The Exploiters
	854510, -- The Glitch Exploiters

	--// SKID GAME GROUPS
	34355680, -- n Da Hood Entertainment
	34363519, -- n Da Hood Stars
	34363528, -- n Da Hood Hall of Fame
	34363527, -- n Da Hood Stars Hall of Fame
	34363691, -- n Da Hood Moderation
	17377873, -- n A Hood Entertainment
	17377876, -- n A Hood Stars
	32318318, -- n Hood Entertainment
	33943813, -- n Da Hood Verified
	16518578, -- Das Hood Entertainment
	14014827, -- Hallowreen (nTBT Copy)
	34386302, -- Hood Arcade Entertainment
	34386310, -- Hood Arcade Stars
	34386306, -- Hood Arcade Verified
	34386309, -- Hood Arcade Moderation
	15067865, -- Universal Hood Entertainment
	34487339, -- Tha Hood Entertainment
	34487348, -- The Hooa Stars
	34463437, -- Dar Hood Entertainment
	34481855, -- Dar Hood Stars

	--// REGULAR GROUPS
	5240969, -- Slvnt Studios
	16144252, -- GORECrew
	11217040, -- Kristina Crew
	9372636, -- Racecodex Crew

}